# Przecinanie się siatek – dokumentacja techniczna
# WYKORZYSTANE TECHNOLOGIE
Java - do realizacji projektu wybrany został język programowania Java. Język ten został
zaprojektowany z myślą o programowaniu obiektowym, co ułatwia stworzenie systemu
łatwego w utrzymaniu i rozwoju. Niestety ze względu na swoją specyfikę i wykonywanie kodu
bajtowego w wirtualnej maszynie Javy, wydajność stworzonej aplikacji może odbiegać od
podobnego programu napisanego w C.
jMonkeyEngine 3.0 – biblioteka do wizualizacji siatek oraz otrzymanych przecięć (części
wspólnej).
Vecmath – biblioteka zawierająca klasę przechowującą wierzchołki, zawiera podstawowe
funkcje dodawania, odejmowania i innych operacji na punktach.
# OPIS KLAS I FUNKCJI
## Klasa Main
Klasa wywołuję metodę simpleInitApp która wczytuje dwie siatki do pamięci przechowując je
w klasie Polygon3D. Siatki mogą być w formacie .ply lub .off, zostanie to rozpoznane przez
funkcję GridLoad, a w przypadku wykrycia nieprawidłowego formatu zostanie wywołany
wyjątek. Następnie zostaje wywołana funkcja clip na obu siatkach, tworzy ona nową siatkę
składającą się z przecięć siatek wczytanych wykorzystując algorytm Sutherlanda-Hodgmana.
Na końcu zostaje wywołana funkcja render klasy Polygon3D która służy do wyświetlania trzech
siatek, dwóch wczytanych oraz jednej utworzonej.
## Klasa Polygon3D
Klasa przechowująca daną siatkę, poprzez trzymanie listy elementów.
Funkcja clip(Polygon3D polygon)
Funkcja znajduje przecięcie podanej siatki, z siatką na której jest wywoływana. Sprawdza po
kolei przecięcia jednej siatki z każdym elementem drugiej. Zwraca nową siatkę, która stanowi
przecięcie obu zadanych siatek.
Funkcja clip(Polygon3D poligon, Face face)
Zwraca elementy stanowiące przecięcia zadanej siatki i elementu. Dla każdego elementu siatki
szuka przecięć z podanym elementem i dodaje je do siatki przecięć.
Następnie „przycinany” jest element zadany przez wszystkie elementy siatki. Gdy pozostanie
takowy na końcu iteracji, dodawany jest do siatki wynikowej. Zapewnia to wypukłość figury.Klasa Face
Element przechowuje wierzchołki. Element powinien być wypukły, składać się przynajmniej z
3 wierzchołków. Kolejność wierzchołków w każdym elemencie powinna być ustawiona w
kolejności przeciwnej do wskazówek zegara patrząc na siatkę z zewnątrz.
## Funkcja rewind
Kolejność wierzchołków w każdym elemencie powinna być ustawiona w kolejności przeciwnej
do wskazówek zegara patrząc na siatkę z zewnątrz. W przypadku wykrycia nieprawidłowego
ustawienia kolejność punktów zostaje odwrócona.
## Funkcja getPointVsFaceDeterminant
Funkcja zwraca wartość wyznacznika macierzy stworzonej z poniżej opisanych punktów.
Wartość tak potrzebna określa czy punkt znajduje się po lewej stronie czy prawej elementu.
Przyjmijmy że element składa się z 3 wierzchołków A, B, C. Mamy również punkt X.
Obliczane są wierzchołki:
```
B`=B-A
C`=C-A
X`=X-A
```
Następnie z otrzymanych wierzchołków liczona jest wartość wyznacznika macierzy złożonej z
tych wektorów
```
B`.x B`.y B`.z
C`.x C`.y C`.z
X`.x X`.y X`.z
```
W przypadku dodatniej wartości wyznacznika punkt znajduje się po lewej stronie elementu
oraz przy ujemnej po prawej. Element jest wypukły dlatego potrzebujemy tylko 3 punktów aby
określić poprawne ustawienie punktu.
Funkcja pointIsInsideFace
Sprawcza czy wartość funkcji getPointVsFaceDeterminant. W przypadku gdy jest ona
mniejsza lub równa 0 to punkt znajduje się po lewej stronie elementu oraz w przypadku
większej wartości od 0 po prawej.
Funkcja getIntersectionPoint
Traktuje element jako nieskończoną powierzchnie, dlatego musi on być wypukły.
Zwraca punkt przecięcia pomiędzy wektorem stworzonym z dwóch punktów.
W przypadku gdy punkty są takie same zwracana jest suma punktów podzielona przez dwa.
Przyjmujemy że:
* Point2 - Punkt końcowy
* Point1 - Punkt początkowy
* determinantPoint1 - wartość wyznacznika punktu1 względem elementu obliczona
przez funkcję getPointVsFaceDeterminant
* determinantPoint2 - wartość wyznacznika punktu2 względem elementu obliczona
przez funkcję getPointVsFaceDeterminant
W celu obliczenia przecięcia należy użyć interpolacji liniowej.
Aby obliczyć punkt przecięcia należy:
* Utworzyć punkt stworzony z różnicy punktu2 oraz punktu1
* Wartości utworzonego punktu pomnożyć przez:
(0-determinantPoint1)/(determinantPoint2-determinantPoint1)
* Dodać do otrzymanego punktu punkt1
Funkcja clipFace
Funkcja zwraca element będący przecięciem dwóch elementów używając algorytmu
Sutherlanda-Hodgmana.Działanie:
Dla każdej krawędzi siatki należy rozpatrzyć 4 przypadki:
* Oba wierzchołki znajdują się wewnątrz elementu – zapisujemy końcowy wierzchołek
* Początkowy wierzchołek jest wewnątrz elementu a końcowy na zewnątrz – zapisujemy
wierzchołek wspólny dla elementu i krawędzi
* Oba wierzchołki są na zewnątrz elementu – nie zapisujemy nic
*Początkowy wierzchołek jest na zewnątrz a końcowy wewnątrz - zapisujemy
wierzchołek wspólny dla elementu i krawędzi oraz wierzchołek końcowy
## OPIS DZIAŁANIA PROGRAMU
W celu utworzenia siatki przecinającej dwa elementy należy stworzyć dwa obiekty klasy
Polygon3D wczytując pliki metodą load klasy GridLoad:
Funkcja load ma możliwość skalowania siatek, i przesuwania ich o wektor w celach testowych.
Poniżej podany jest przykład przecięcia dwóch takich samych siatek:
Następnie po wczytaniu zmiennych należy wywołać metodę „clip” na obiekcie siatki podając
jako argument obiekt drugiej siatki. Zostanie utworzony nowy obiekt klasy Polygon3D
zawierający przecięcie siatek:
```
Polygon3D polygon2 = gridLoad.load("canstick.ply.txt");
Polygon3D polygon = gridLoad.load("canstick.ply.txt");
Vector3d scale=new Vector3d();
Vector3d scale2=new Vector3d();
Vector3d translation=new Vector3d();
Vector3d translation2=new Vector3d();
scale.x = 100.;
scale.y = 100.;
scale.z = 100.;
translation.x = -5.;
translation.y = 0.;
translation.z = 0.;
scale2.x = 50;
scale2.y = 50;
scale2.z = 50;
translation2.x = -15.1;
translation2.y = -5.5;
translation2.z = 0.5;
```
```
Polygon3D polygon2 = gridLoad.load("canstick.ply.txt", scale, translation);
Polygon3D polygon = gridLoad.load("canstick.ply.txt", scale2, translation2);
Polygon3D intersectionPolygon = polygon2.clip(polygon);
```
W celach wizualizacyjnych klasa Polygon3D posiada metodę render, która wyświetla siatkę w
danym kolorze zależnie od typu wyliczeniowego ColorRGBA. Poniższa metoda wyświetla trzy
siatki w kolorze zielonym, niebieskim oraz czerwonym:
Istnieje również możliwość zapisu utworzonej siatki do pliku w formacie PLY lub OFF. Należy
jedynie wywołać metodę save podając jako parametry nazwę pliku lub nazwę z lokalizacją oraz
format w postaci Format.PLY lub Format.OFF.
Pakiet Formats zawiera klasę abstrakcyjną służącą do zapisu oraz wczytywania siatek w
różnych formatach. Aby dodać mozliwość wczytywania nowych formatów należy rozszerzyć
klasę SaveFormat, a w przypadku zapisu LoadFormat. Następnie należy zaimplementować
zawarte w niej metody.
Pseudokod zaimplementowanego algorytmu
Pseudokod rozwiązania
## DODATKOWA DOKUMENTACJA
Zaimplementowane funkcje zostały opisane i znajdują się w folderze “Dokumentacja” w
repozytorium kodu. W celu otworzenia dokumentacji należy skorzystać z pliku “index.html”.
## REFERENCJE
## Funkcja clipFace
https://en.wikipedia.org/wiki/Sutherland–Hodgman_algorithm Analityczny algorytm
obcinania

```
polygon2.render(assetManager, rootNode, ColorRGBA.Green);
polygon.render(assetManager, rootNode, ColorRGBA.Blue);
intersectionPolygon.render(assetManager, rootNode, ColorRGBA.Red);
polygon.save("intersected.off", Format.PLY);
for each clippiING face of clippingPolygon
for each face of clippEDPolygon
clipp face by clippingFace
end
end
for each clippiING face of clippingPolygon
for each face of clippEDPolygon
clipp face by clippingFace
end
end
```

## Funkcja pointIsInsideFace
http://math.stackexchange.com/questions/214187/point-on-the-left-or-right-side-of-aplane-in-3d-space Pozycja punktu względem elementu
http://www.mathsisfun.com/algebra/matrix-determinant.html Wyznacznik macierzy
elementu
## Funkcja getIntersectionPoint
http://mathworld.wolfram.com/Plane.html Wyznaczanie punktu przecięcia krawędzi z
elementem