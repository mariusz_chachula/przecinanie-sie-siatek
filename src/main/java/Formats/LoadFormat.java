package Formats;

import javax.vecmath.Vector3d;

import Engine.PolygonStatistics;

/**
 * Klasa abstakcyjna zawierająca metody do wczytywania siatki z pliku
 */
public class LoadFormat {
	

	protected static void compare(PolygonStatistics faceMinMax, Vector3d vector) {
		// x
		if (vector.x > faceMinMax.maxX)
			faceMinMax.maxX = vector.x;

		if (vector.x < faceMinMax.minX)
			faceMinMax.minX = vector.x;

		// y
		if (vector.y > faceMinMax.maxY)
			faceMinMax.maxY = vector.y;

		if (vector.y < faceMinMax.minY)
			faceMinMax.minY = vector.y;

		// z
		if (vector.z > faceMinMax.maxZ)
			faceMinMax.maxZ = vector.z;

		if (vector.z < faceMinMax.minZ)
			faceMinMax.minZ = vector.z;
	}
}
