package Formats;

import java.io.BufferedReader;
import java.io.IOException;

import javax.vecmath.Vector3d;

import Grid.Polygon3D;

public interface LoadFileInterface {
	public Polygon3D load(BufferedReader br, Vector3d vector, Vector3d translation)
			throws NumberFormatException, IOException;

}
