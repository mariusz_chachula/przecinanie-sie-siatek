package Formats;


import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.vecmath.Vector3d;
import Grid.Face;
import Grid.Polygon3D;

/**
 * Zapis siatki do pliku w formacie OFF
 */
public class SaveOFF extends SaveFormat {


	public void save(String fileName, Polygon3D polygon) throws IOException {
		this.polygon=polygon;
		
		System.out.println("Zapis siatki do pliku");
		Set<Vector3d> points = new HashSet<Vector3d>();
		int numberOfEdges = 0;

		for (Face face : polygon.getFaces()) {
			for (int i = 0; i < face.getVerticles().size(); i++) {
				// Krawedz istnieje gdy dwa nastepne wierzcholki nie sa dodane
				// do listy
				if (!points.contains(face.getVerticles().get(i))
						&& !points.contains(face.getVerticles().get(i % face.getVerticles().size()))) {
					numberOfEdges += face.getVerticles().size();
				}
			}

			points.addAll(face.getVerticles());
		}

		File polygonFile = new File(fileName);

		if (polygonFile.exists()) {
			System.out.println("Taki plik już istnieje. Nadpisywanie");
			polygonFile.delete();
			polygonFile.createNewFile();
		}

		polygonFile.createNewFile();
		List<Vector3d> vectors = new LinkedList<>();
		
		saveFile(header(points, numberOfEdges), polygonFile);
		saveFile(points(points,vectors), polygonFile);
		saveFile(faces(vectors), polygonFile);

		System.out.println("Zakończono zapis do pliku");
	}


	protected String header(Set<Vector3d> points, int numberOfEdges) {
		// header
		StringBuilder header = new StringBuilder();

		header.append("OFF");
		header.append("\n");

		header.append(points.size());
		header.append(" ");
		header.append(polygon.getFaces().size());
		header.append(" ");
		header.append(numberOfEdges);
		header.append("\n");

		return header.toString();
	}


	protected String faces(List<Vector3d> vectors) {
		StringBuilder facesText = new StringBuilder();

		for (Face face : polygon.getFaces()) {
			facesText.append(face.getVerticles().size());
			for (Vector3d vector : face.getVerticles()) {
				facesText.append(" ");
				facesText.append(vectors.indexOf(vector));
			}
			facesText.append("\n");
		}
		return facesText.toString();
	}


	protected String points(Set<Vector3d> points, List<Vector3d> vectors) {
		Iterator<Vector3d> iterator = points.iterator();
		StringBuilder pointsText = new StringBuilder();

		while (iterator.hasNext()) {
			Vector3d vector = iterator.next();
			vectors.add(vector);
			pointsText.append(vector.x);
			pointsText.append(" ");
			pointsText.append(vector.y);
			pointsText.append(" ");
			pointsText.append(vector.z);
			pointsText.append("\n");
		}
		
		return pointsText.toString();
	}
}
