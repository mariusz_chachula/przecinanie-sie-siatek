package Formats;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.vecmath.Vector3d;

import Engine.Main;
import Engine.PolygonStatistics;
import Grid.Face;
import Grid.Polygon3D;
import Grid.Print;

/**
 * Wczytanie siatki z pliku w formacie OFF
 */
public class LoadOFF extends LoadFormat{

	public List<Vector3d> readPoints(BufferedReader br, int pointsCount, PolygonStatistics faceMinMax)
			throws IOException {
		List<Vector3d> pointsL = new LinkedList<>();

		String[] points;
		int i = 0;
		while (pointsCount > i++) {
			points = br.readLine().split(" ");
			Vector3d point = new Vector3d();
			point.x = Double.parseDouble(points[0]);
			point.y = Double.parseDouble(points[1]);
			point.z = Double.parseDouble(points[2]);
			pointsL.add(point);

			if (i == 0) {
				faceMinMax.maxX = point.x;
				faceMinMax.minX = point.x;

				faceMinMax.maxY = point.y;
				faceMinMax.minY = point.y;

				faceMinMax.minZ = point.z;
				faceMinMax.minZ = point.z;
			} else {
				compare(faceMinMax, point);
			}
		}
		return pointsL;
	}
	
	/**
	 * Wczytywanie punktów
	 * 
	 * @param br
	 * @param pointsCount
	 * @return
	 * @throws IOException
	 */
	private static List<Vector3d> readPoints(BufferedReader br, int pointsCount, Vector3d vector, Vector3d transl,
			PolygonStatistics faceMinMax) throws IOException {
		List<Vector3d> pointsL = new LinkedList<>();

		String[] points = null;
		int i = 0;
		while (pointsCount > i++) {
			points = br.readLine().split(" ");
			Vector3d point = new Vector3d();
			point.x = Double.parseDouble(points[0]) / vector.x + transl.x;
			point.y = Double.parseDouble(points[1]) / vector.y + transl.y;
			point.z = Double.parseDouble(points[2]) / vector.z + transl.z;

			if (i == 1) {
				faceMinMax.maxX = point.x;
				faceMinMax.minX = point.x;

				faceMinMax.maxY = point.y;
				faceMinMax.minY = point.y;

				faceMinMax.minZ = point.z;
				faceMinMax.minZ = point.z;
			} else {
				compare(faceMinMax, point);
			}

			pointsL.add(point);
		}

		System.out.println("Wartości skrajne: ");
		System.out.println("xMin: " + faceMinMax.minX);
		System.out.println("xMax: " + faceMinMax.maxX);

		System.out.println("yMin: " + faceMinMax.minY);
		System.out.println("yMax: " + faceMinMax.maxY);

		System.out.println("zMin: " + faceMinMax.minZ);
		System.out.println("zMax: " + faceMinMax.maxZ);

		return pointsL;
	}
	/**
	 * Wczytywanie elementów z pliku
	 * 
	 * @param br
	 * @param facesCount
	 * @param vectors
	 * @return
	 * @throws IOException
	 */
	 protected Polygon3D readFaces(BufferedReader br, int facesCount, List<Vector3d> vectors) throws IOException {
		String[] faces;
		Polygon3D polygon = new Polygon3D();

		int i = 0;
		while (facesCount > i++ && (faces = br.readLine().split(" +")) != null) {

			Face face = new Face();

			for (int j = 0; j < Integer.parseInt(faces[0]); j++) {
				face.addVertex(vectors.get(Integer.parseInt(faces[j + 1])));
			}

			polygon.addFace(face);
		}

		return polygon;

	}
	 
	 /**
		 * Wczytywanie plików typu off
		 * 
		 * @param br
		 * @return Lista elementów
		 * @throws NumberFormatException
		 * @throws IOException
		 */

		 public Polygon3D load(BufferedReader br, Vector3d vector, Vector3d transl)
				throws NumberFormatException, IOException {
			String line;

			int pointsCount = 0;
			int facesCount = 0;
			String[] points;

			line = br.readLine();
			// punkty sciany
			points = line.split(" ");
			pointsCount = Integer.parseInt(points[0]);
			facesCount = Integer.parseInt(points[1]);

			if (Main.print)
				System.out.println("pointsCount: " + pointsCount + " facesCount:" + facesCount);

			List<Vector3d> pointsL;
			PolygonStatistics stats = new PolygonStatistics();

			if (vector != null)
				pointsL = readPoints(br, pointsCount, vector, transl, stats);
			else
				pointsL = readPoints(br, pointsCount, stats);

			if (Main.print)
				Print.printPoints(pointsL);

			Polygon3D polygon = readFaces(br, facesCount, pointsL);
			polygon.setStatistics(stats);

			return polygon;
		}
	
}