package Formats;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;

import javax.vecmath.Vector3d;

import Engine.PolygonStatistics;
import Grid.Polygon3D;

/**
 * Klasa abstakcyjna zawierająca metody do zapisywania siatki do pliku
 */
public abstract class SaveFormat implements SaveToFileInterface{
	
	public SaveFormat()
	{
		
	}
	
	/** 
	 * Inicjalizacja klasy na siatle polygon
	 */
	public SaveFormat(Polygon3D polygon)
	{
		this.polygon=polygon;
	}
	
	Polygon3D polygon = null;
	
	
	protected void compare(PolygonStatistics faceMinMax, Vector3d vector) {
		// x
		if (vector.x > faceMinMax.maxX)
			faceMinMax.maxX = vector.x;

		if (vector.x < faceMinMax.minX)
			faceMinMax.minX = vector.x;

		// y
		if (vector.y > faceMinMax.maxY)
			faceMinMax.maxY = vector.y;

		if (vector.y < faceMinMax.minY)
			faceMinMax.minY = vector.y;

		// z
		if (vector.z > faceMinMax.maxZ)
			faceMinMax.maxZ = vector.z;

		if (vector.z < faceMinMax.minZ)
			faceMinMax.minZ = vector.z;
	}
	
	/**
	 * Zapis tekstu do pliku w nowej linijce
	 * 
	 * @param text
	 *            Tekst
	 * @param file
	 *            Plik do którego zostanie zapisany tekst
	 */
	protected void saveFile(String text, File file) {

		try (PrintWriter output = new PrintWriter(new FileWriter(file, true))) {
			output.printf(text, "\r\n");
		} catch (Exception e) {
		}

	}
}
