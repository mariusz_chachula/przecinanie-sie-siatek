package Formats;

import java.io.IOException;

import Grid.Polygon3D;

public interface SaveToFileInterface {

	/**
	 * Metoda wykonująca zapis do pliku
	 * 
	 * @param fileName
	 *            Nazwa pliku
	 * @param polygon
	 *            Siatka ktora zostanie zapisana
	 * @throws IOException
	 */
	public void save(String fileName, Polygon3D polygon) throws IOException;
}
