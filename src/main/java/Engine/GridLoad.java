package Engine;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.vecmath.Vector3d;

import Formats.LoadOFF;
import Formats.LoadPLY;
import Grid.Polygon3D;

/**
 * Wczytywanie plików w różnych formatach
 */
public class GridLoad {

	/**
	 * Wczytywanie plików typu ply
	 * 
	 * @param br
	 * @return Lista elementów
	 * @throws NumberFormatException
	 * @throws IOException
	 */
	static Polygon3D loadPly(BufferedReader br, Vector3d vector, Vector3d translation)
			throws NumberFormatException, IOException {
		LoadPLY loadPLY = new LoadPLY();
		return loadPLY.load(br, vector, translation);
	}

	public static Polygon3D rewindPolygon(Polygon3D polygon) {
		PolygonStatistics polygonProp = polygon.getStatistics();

		Vector3d center = new Vector3d(0.5 * (polygonProp.minX + polygonProp.maxX),
				0.5 * (polygonProp.minY + polygonProp.maxY), 0.5 * (polygonProp.minZ + polygonProp.maxZ));
		System.out.println("Center: " + center);
		for (int i = 0; i < polygon.getNumberOfFaces(); i++) {
			polygon.getFace(i).rewind(center);
		}

		return polygon;
	}

	public Polygon3D load(String filePath, Vector3d vector, Vector3d translation)

			throws FileNotFoundException, IOException {
		String format = null;
		try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {

			if ((format = br.readLine().toLowerCase()).equals("off")) {
				LoadOFF loadOFF = new LoadOFF();
				return rewindPolygon(loadOFF.load(br, vector, translation));

			} else if (format.equals("ply")) {
				LoadPLY loadPLY = new LoadPLY();
				return rewindPolygon(loadPLY.load(br, vector, translation));
			} else {
				System.out.println("Zły format");
				return null;
			}
		}
	}

	public Polygon3D load(String filePath) throws FileNotFoundException, IOException {
		Vector3d v = new Vector3d();
		v.x = 1.;
		v.y = 1.;
		v.z = 1.;
		return load(filePath, v, v);
	}

}
