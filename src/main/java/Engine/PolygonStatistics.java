package Engine;

/**
 * Podstawowe statystyki dotyczące siatki
 */
public class PolygonStatistics {
	public double minX;
	public double maxX;

	public double minY;
	public double maxY;

	public double minZ;
	public double maxZ;

	public void average(PolygonStatistics pol) {

		minX = (minX + pol.minX) / 2.;
		maxX = (maxX + pol.maxX) / 2.;

		minY = (minY + pol.minY) / 2.;
		maxY = (maxY + pol.maxY) / 2.;

		minZ = (minZ + pol.minZ) / 2.;
		maxZ = (maxZ + pol.maxZ) / 2.;

	}
}
