package Engine;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.vecmath.Vector3d;

//Biblioteki do wizualizacji
import com.jme3.app.SimpleApplication;
import com.jme3.math.ColorRGBA;

import Formats.Format;
import Grid.Face;
import Grid.Polygon3D;

/**
 * Klasa uruchomieniowa
 */
public class Main extends SimpleApplication {

	public static Boolean print = false;

	public static void main(String[] args) {
		Main app = new Main();
		app.start();
	}

	public void candleStick(Vector3d scale, Vector3d scale2, Vector3d translation, Vector3d translation2) {
		scale.x = 0.2;
		scale.y = 0.2;
		scale.z = 0.2;

		translation.x = 7.6;
		translation.y = 0.;
		translation.z = 0.;

		scale2.x = 0.2;
		scale2.y = 0.2;
		scale2.z = 0.2;

		translation2.x = 7.7;
		translation2.y = 0.;
		translation2.z = 0.;
	}

	public void cube(Vector3d scale, Vector3d scale2, Vector3d translation, Vector3d translation2) {
		scale.x = 1.;
		scale.y = 1.;
		scale.z = 1.;

		translation.x = 7.;
		translation.y = 0.;
		translation.z = 0.;

		scale2.x = 1;
		scale2.y = 1;
		scale2.z = 1;

		translation2.x = 6.1;
		translation2.y = 0.5;
		translation2.z = 0.5;
	}

	public void sphere(Vector3d scale, Vector3d scale2, Vector3d translation, Vector3d translation2) {
		scale.x = 100.;
		scale.y = 100.;
		scale.z = 100.;

		translation.x = 6.;
		translation.y = 0.;
		translation.z = 0.;

		scale2.x = 100;
		scale2.y = 100;
		scale2.z = 100;

		translation2.x = 6.1;
		translation2.y = 0.5;
		translation2.z = 0.5;
	}

	public void airplane(Vector3d scale, Vector3d scale2, Vector3d translation, Vector3d translation2) {
		scale.x = 100.;
		scale.y = 100.;
		scale.z = 100.;

		translation.x = -5.;
		translation.y = 0.;
		translation.z = 0.;

		scale2.x = 50;
		scale2.y = 50;
		scale2.z = 50;

		translation2.x = -15.1;
		translation2.y = -5.5;
		translation2.z = 0.5;

	}

	@Override
	public void simpleInitApp() {
		GridLoad gridLoad = new GridLoad();
		Polygon3D polygon2;
		Polygon3D polygon;
		try {

			Vector3d scale = new Vector3d();
			Vector3d translation = new Vector3d();

			Vector3d scale2 = new Vector3d();
			Vector3d translation2 = new Vector3d();

			/*
			 * candleStick(scale, scale2, translation, translation2); polygon2 =
			 * gridLoad.load("Grids"+File.pathSeparator+"canstick.ply.txt", scale, translation); polygon =
			 * gridLoad.load("Grids"+File.pathSeparator+"canstick.ply.txt", scale2, translation2);
			 * 
			 * cube(scale, scale2, translation, translation2); polygon2 =
			 * gridLoad.load("Grids"+File.pathSeparator+"cube.ply", scale, translation); polygon =
			 * gridLoad.load("Grids"+File.pathSeparator+"cube.ply", scale2, translation2);
			 * 
			 * sphere(scale, scale2, translation, translation2); polygon2 =
			 * gridLoad.load("Grids"+File.pathSeparator+"sphere.ply", scale, translation); polygon =
			 * gridLoad.load("Grids"+File.pathSeparator+"sphere.ply", scale2, translation2);
			 */
			airplane(scale, scale2, translation, translation2);
			polygon2 = gridLoad.load("Grids"+File.pathSeparator+"airplane.ply", scale, translation2);
			polygon = gridLoad.load("Grids"+File.pathSeparator+"sphere.ply", scale, translation);

			Polygon3D intersectionPolygon = polygon2.clip(polygon);

			polygon2.render(assetManager, rootNode, ColorRGBA.Green);
			polygon.render(assetManager, rootNode, ColorRGBA.Blue);
			intersectionPolygon.render(assetManager, rootNode, ColorRGBA.Red);

			polygon.save("Grids"+File.pathSeparator+"intersected.off", Format.PLY);

			/*
			 * PolygonStatistics polygonProp = cube.getStatistics();
			 * PolygonStatistics polygonProp2 = cube.getStatistics();
			 * 
			 * polygonProp.average(polygonProp2);
			 */

		} catch (FileNotFoundException e) {
			System.out.println("File not found");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Error in reading file");
			e.printStackTrace();
		}
	}

	public Polygon3D getCubePolygon(Vector3d mins, Vector3d maxs, double xSkew) {
		Vector3d hhh = new Vector3d(maxs.x + xSkew, maxs.y, maxs.z);
		Vector3d hhl = new Vector3d(maxs.x + xSkew, maxs.y, mins.z);
		Vector3d hlh = new Vector3d(maxs.x - xSkew, mins.y, maxs.z);
		Vector3d hll = new Vector3d(maxs.x - xSkew, mins.y, mins.z);
		Vector3d lhh = new Vector3d(mins.x + xSkew, maxs.y, maxs.z);
		Vector3d lhl = new Vector3d(mins.x + xSkew, maxs.y, mins.z);
		Vector3d llh = new Vector3d(mins.x - xSkew, mins.y, maxs.z);
		Vector3d lll = new Vector3d(mins.x - xSkew, mins.y, mins.z);

		Vector3d centre = new Vector3d(0.5 * (mins.x + maxs.x), 0.5 * (mins.y + maxs.y), 0.5 * (mins.z + maxs.z));

		Face top = new Face();
		Face bottom = new Face();
		Face north = new Face();
		Face south = new Face();
		Face east = new Face();
		Face west = new Face();

		north.addVertex(hll);
		north.addVertex(hhl);
		north.addVertex(hhh);
		north.addVertex(hlh);
		north.rewind(centre);

		south.addVertex(lll);
		south.addVertex(lhl);
		south.addVertex(lhh);
		south.addVertex(llh);
		south.rewind(centre);

		top.addVertex(hhh);
		top.addVertex(hhl);
		top.addVertex(lhl);
		top.addVertex(lhh);
		top.rewind(centre);

		bottom.addVertex(hlh);
		bottom.addVertex(hll);
		bottom.addVertex(lll);
		bottom.addVertex(llh);
		bottom.rewind(centre);

		east.addVertex(hhh);
		east.addVertex(hlh);
		east.addVertex(llh);
		east.addVertex(lhh);
		east.rewind(centre);

		west.addVertex(hhl);
		west.addVertex(hll);
		west.addVertex(lll);
		west.addVertex(lhl);
		west.rewind(centre);

		Polygon3D poly = new Polygon3D();
		poly.addFace(top);
		poly.addFace(bottom);
		poly.addFace(north);
		poly.addFace(south);
		poly.addFace(east);
		poly.addFace(west);

		return poly;
	}

	public Polygon3D getGridPolygon(Vector3d mins, Vector3d maxs, double xSkew) {
		Vector3d hhh = new Vector3d(maxs.x + xSkew, maxs.y, maxs.z);
		Vector3d hhl = new Vector3d(maxs.x + xSkew, maxs.y, mins.z);
		Vector3d hlh = new Vector3d(maxs.x - xSkew, mins.y, maxs.z);
		Vector3d hll = new Vector3d(maxs.x - xSkew, mins.y, mins.z);

		Vector3d centre = new Vector3d(0.5 * (mins.x + maxs.x), 0.5 * (mins.y + maxs.y), 0.5 * (mins.z + maxs.z));

		Face top = new Face();

		top.addVertex(hll);
		top.addVertex(hhl);
		top.addVertex(hhh);
		top.addVertex(hlh);
		top.rewind(centre);

		Polygon3D poly = new Polygon3D();
		poly.addFace(top);

		return poly;
	}
}