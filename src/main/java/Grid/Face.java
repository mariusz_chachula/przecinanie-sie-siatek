package Grid;

import java.util.ArrayList;
import java.util.List;

import javax.vecmath.Vector3d;

import com.jme3.asset.AssetManager;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.VertexBuffer;

/**
 * Klasa przechowująca i modyfikująca wierzchołki elementu
 */
public class Face {

	// Lista krawędzi
	ArrayList<Vector3d> verticies = new ArrayList<Vector3d>();

	/**
	 * Konstruktor dodający wierzchołki do listy
	 * 
	 * @param vectors
	 *            Lista wierzchołków tworzących element
	 */
	public Face(Vector3d... vectors) {
		for (Vector3d vector : vectors) {
			verticies.add(vector);
		}
	};
	
	
	public List<Vector3d> getVerticles()
	{
		return verticies;
	}

	/**
	 * Konstruktor zmieniający listę wierzchołków
	 * 
	 * @param verticies
	 *            Lista wierzchołków
	 */
	public Face(ArrayList<Vector3d> verticies) {
		this.verticies = verticies;
	}

	/**
	 * Zmiana położenia każdego wierzchołka o wektor
	 * 
	 * @param vector
	 *            Wektor zmieniający położenie wierzchołków w elemencie
	 */
	public void moveVertex(Vector3d vector) {

		for (int i = 0; i < getNumberOfEdges(); i++) {
			getVertex(i).x = getVertex(i).x + vector.x;
			getVertex(i).y = getVertex(i).y + vector.y;
			getVertex(i).z = getVertex(i).z + vector.z;

		}

	}

	/**
	 * Dodawanie wierzchołka do elementu
	 * 
	 * @param vertex
	 *            Dodawany wierzchołek
	 */
	public void addVertex(Vector3d vertex) {
		if (Double.isNaN(vertex.x)) {
			throw new RuntimeException("NaN Vertex");
		}
		if (Double.isInfinite(vertex.x) || Double.isInfinite(vertex.y) || Double.isInfinite(vertex.z)) {
			throw new RuntimeException("infinite Vertex");
		}

		if (!verticies.contains(vertex)) {
			verticies.add(vertex);
		}

	}

	public void rewind(Vector3d internalPoint) {
		if (pointIsInsideFace(internalPoint) == false) {
			ArrayList<Vector3d> verticiesRewound = new ArrayList<Vector3d>(verticies.size());
			for (int i = verticies.size() - 1; i >= 0; i--) {
				verticiesRewound.add(verticies.get(i));
			}
			verticies = verticiesRewound;
		}
	}

	/**
	 * Liczbą wierzchołków
	 * 
	 * @return Liczba wierzchołków
	 */
	public int getNumberOfEdges() {
		return verticies.size();
	}

	/**
	 * Funkcja zwracająca dany wierzchołek
	 * 
	 * @param vertex
	 *            Numer wierzchołka w liście
	 * @return wierzchołek
	 */
	public Vector3d getVertex(int vertex) {
		return verticies.get(vertex);
	}

	/**
	 * Funkcja zwarająca początek wierzchołka
	 * 
	 * @param edgeNo
	 *            numer wierzchołka
	 * @return wierzchołek
	 */
	public Vector3d getStartOfEdge(int edgeNo) {
		return verticies.get(edgeNo);
	}

	/**
	 * Funkcja zwracająca koniec wierzchołka
	 * 
	 * @param edgeNo
	 *            numer wierzchołka
	 * @return wierzchołek
	 */
	public Vector3d getEndOfEdge(int edgeNo) {
		return verticies.get((edgeNo + 1) % verticies.size());
	}

	private double getPointVsFaceDeterminant(Vector3d point) {

		if (verticies.size() < 3) {
			throw new RuntimeException("Element ma mniej niż 3 wierzchołki");
		}

		Vector3d a = verticies.get(0);
		Vector3d b = verticies.get(1);
		Vector3d c = verticies.get(2);

		Vector3d x = point;

		Vector3d bDash = new Vector3d();
		bDash.sub(b, a);

		Vector3d cDash = new Vector3d();
		cDash.sub(c, a);

		Vector3d xDash = new Vector3d();
		xDash.sub(x, a);

		double determinant = bDash.x * (cDash.y * xDash.z - cDash.z * xDash.y)
				- bDash.y * (cDash.x * xDash.z - cDash.z * xDash.x) + bDash.z * (cDash.x * xDash.y - cDash.y * xDash.x);

		return determinant;
	}

	private boolean pointIsInsideFace(Vector3d point) {
		double determinant = getPointVsFaceDeterminant(point);
		if (determinant <= 0.) {
			return true;
		} else {
			return false;
		}
	}


	private Vector3d getIntersectionPoint(Vector3d rayPoint1, Vector3d rayPoint2) {

		double determinantPoint1 = getPointVsFaceDeterminant(rayPoint1);
		double determinantPoint2 = getPointVsFaceDeterminant(rayPoint2);

		if (determinantPoint1 == determinantPoint2) {
			Vector3d average = new Vector3d();
			average.add(rayPoint1, rayPoint2);
			average.scale(0.5);
			System.out.println("Punkt przeciecia sredni: " + average);
			return average;
		} else {
			Vector3d intersect = new Vector3d();
			intersect.sub(rayPoint2, rayPoint1);
			intersect.scale((0 - determinantPoint1) / (determinantPoint2 - determinantPoint1));
			intersect.add(rayPoint1);
			return intersect;
		}
	}

	public Face clipFace(Face clippingFace, Face intersectionFace) {
		Face workingFace = new Face();

		for (int i = 0; i < this.getNumberOfEdges(); i++) {

			Vector3d point1 = getStartOfEdge(i);
			Vector3d point2 = getEndOfEdge(i);

			if (clippingFace.pointIsInsideFace(point1) && clippingFace.pointIsInsideFace(point2)) {
				// 1) oba wierzchołki
				// znajdują się wewnątrz elementu – // zapisujemy końcowy
				// wierzchołek

				workingFace.addVertex(point2);
			} else if (clippingFace.pointIsInsideFace(point1) && clippingFace.pointIsInsideFace(point2) == false) { // początkowy
				// wierzchołek jest wewnątrz elementu a końcowy // na //
				// zewnątrz
				// – zapisujemy wierzchołek wspólny dla elementu i // krawędzi

				Vector3d intersection = clippingFace.getIntersectionPoint(point1, point2);
				workingFace.addVertex(intersection);
				intersectionFace.addVertex(intersection);
			} else if (clippingFace.pointIsInsideFace(point1) == false
					&& clippingFace.pointIsInsideFace(point2) == false) {
				// Oba wierzchołki są na zewnątrz elementu – nie zapisujemy //
				// nic

			} else { // początkowy wierzchołek jest na zewnątrz a końcowy //
				// wewnątrz - // zapisujemy wierzchołek wspólny dla elementu i
				// krawędzi // oraz wierzchołek końcowy

				Vector3d intersection = clippingFace.getIntersectionPoint(point1, point2);
				intersectionFace.addVertex(intersection);
				workingFace.addVertex(intersection);
				workingFace.addVertex(point2);
			}

		}

		if (workingFace.getNumberOfEdges() >= 3) {

			return workingFace;

		}
		return null;

	}

	/**
	 * Wizualizacja
	 * 
	 * @param assetManager
	 * @param color
	 * @return
	 */
	public Geometry getGeometry(AssetManager assetManager, ColorRGBA color) {
		Mesh m = new Mesh();
		m.setMode(Mesh.Mode.LineLoop);

		float[] verticiesForBuffer = new float[3 * (getNumberOfEdges())];
		int[] indicies = new int[getNumberOfEdges()];
		for (int i = 0; i < getNumberOfEdges(); i++) {
			Vector3d vertex = getVertex(i);
			verticiesForBuffer[i * 3] = (float) vertex.x;
			verticiesForBuffer[i * 3 + 1] = (float) vertex.y;
			verticiesForBuffer[i * 3 + 2] = (float) vertex.z;
			indicies[i] = i;
		}

		m.setBuffer(VertexBuffer.Type.Position, 3, verticiesForBuffer);
		m.setBuffer(VertexBuffer.Type.Index, 1, indicies);

		m.updateBound();
		m.updateCounts();

		Geometry geom = new Geometry("Box", m);

		Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		mat.setColor("Color", color);
		geom.setMaterial(mat);

		return geom;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		for (Vector3d vector : verticies) {
			sb.append(vector);
			sb.append("\n");
		}

		return sb.toString();
	}

}