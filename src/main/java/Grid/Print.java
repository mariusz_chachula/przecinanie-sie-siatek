package Grid;

import java.util.List;

import javax.vecmath.Vector3d;


/**
 * Klasa przeznaczona do wyświetlania elementów siatki oraz ich wierzchołków
 */
public class Print<T> {

	/**
	 * Wyswietlanie listy wierzcholkow
	 */
	public static void printPoints(List<Vector3d> pointsL) {
		for (Object point : pointsL) {
			System.out.println(point);
		}
	}

	/**
	 * Wyswietlanie listy elementow
	 */
	public static void printFaceList(List<Face> faceList) {
		for (Face point : faceList) {
			System.out.println(point);
		}
	}

}
