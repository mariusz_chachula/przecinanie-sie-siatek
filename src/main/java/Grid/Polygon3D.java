package Grid;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.vecmath.Vector3d;

import com.jme3.asset.AssetManager;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;

import Engine.PolygonStatistics;
import Formats.Format;
import Formats.SaveOFF;
import Formats.SavePLY;
import Formats.SaveToFileInterface;


/**
 * Klasa zarządzająca elementami siatki
 */
public class Polygon3D {

	public static List<Vector3f> facesInter = new LinkedList<>();

	ArrayList<Face> faces = new ArrayList<Face>();
	private PolygonStatistics statistics = new PolygonStatistics();

	public Polygon3D() {
	}

	public PolygonStatistics getStatistics() {
		return statistics;
	}

	public void setStatistics(PolygonStatistics statistics) {
		this.statistics = statistics;
	}

	public Polygon3D(ArrayList<Face> faces) {
		this.faces = faces;
	}

	/**
	 * Liczba elementów
	 */
	public int getNumberOfFaces() {
		return faces.size();
	}

	/** Dodawanie elementu do siatki **/
	public void addFace(Face face) {
		faces.add(face);
	}

	/** Pobranie elementu z siatki **/
	public Face getFace(int faceNumber) {
		return faces.get(faceNumber);
	}

	public Polygon3D clip(Polygon3D clippingPolygon) {
		Polygon3D workingPolygon = this;
		Face intersectionFace = new Face();

		for (int j = 0; j < clippingPolygon.getNumberOfFaces(); j++) {
			workingPolygon = clip(workingPolygon, clippingPolygon.getFace(j), intersectionFace);
		}

		Polygon3D out = new Polygon3D();
		out.addFace(intersectionFace);
		return workingPolygon;
	}

	private Polygon3D clip(Polygon3D inPolygon, Face clippingFace, Face intersectionFace) {
		Polygon3D outPolygon = new Polygon3D();

		for (int i = 0; i < inPolygon.getNumberOfFaces(); i++) {
			Face clippedFace = inPolygon.getFace(i).clipFace(clippingFace, intersectionFace);
			if (clippedFace != null) {
				outPolygon.addFace(clippedFace);
			}
		}

		return outPolygon;
	}

	/**
	 * Przesunięcie punktów siatki o wektor
	 * 
	 * @param vector
	 */
	public void moveByVector(Vector3d vector) {
		for (int i = 0; i < getNumberOfFaces(); i++) {
			getFace(i).moveVertex(vector);
		}
	}

	/**
	 * Wizualizacja siatki
	 */
	public void render(AssetManager assetManager, Node node, ColorRGBA color) {
		for (int i = 0; i < getNumberOfFaces(); i++) {
			node.attachChild(getFace(i).getGeometry(assetManager, color));
		}
	}

	public ArrayList<Face> getFaces() {
		return faces;
	}

	/**
	 * Funkcja zapisująca siatkę do pliku w formacie PLY lub OFF
	 * 
	 * @param fileName
	 *            nazwa pliku
	 * @param format
	 *            format PLY lub OFF
	 */
	public void save(String fileName, Format format) throws IOException {
		SaveToFileInterface save = null;

		if (format.equals(Format.PLY)) {
			save = new SavePLY();
		} else if (format.equals(Format.OFF)) {
			save = new SaveOFF();
		}
		save.save(fileName, this);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < getNumberOfFaces(); i++) {
			sb.append(getFace(i));
			sb.append("\n");
		}

		return sb.toString();
	}

}